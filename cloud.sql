-- MySQL dump 10.13  Distrib 5.7.30, for Linux (x86_64)
--
-- Host: localhost    Database: cloud
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `factura` (
  `IdFactura` int(11) NOT NULL AUTO_INCREMENT,
  `titular` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `fecha` date NOT NULL,
  `total` int(11) NOT NULL,
  PRIMARY KEY (`IdFactura`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
INSERT INTO `factura` VALUES (1,'Carlos Diaz','calle 25','2020-05-10',16000),(2,'Karen Gil','cra 56','2020-05-10',14000),(4,'Lina Rios','calle 23','2020-05-11',18000),(5,'Laura Alba','cra 14','2020-05-12',10000),(6,'Maria Perez','calle 100','2020-05-14',4000),(7,'Maria Perez','Cra 45','2020-05-14',9000),(8,'Maria Perez','calle 40','2020-05-14',9000),(9,'Sofia Leon','Cra 45','2020-05-14',22000),(10,'Ana Barrera','call 23','2020-05-18',19000),(11,'Ramon Osorio','cra 200','2020-05-18',13000),(12,'Fabian Morales','cll 34','2020-05-18',20000),(13,'Angie Diaz','cll 1200','2020-05-18',24000),(14,'Julian Flores','cll 12','2020-05-18',8000),(15,'Fabian Morales','calle 40','2020-05-19',20000),(16,'Fabian Morales','cll 30 ','2020-05-19',12500);
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacion`
--

DROP TABLE IF EXISTS `facturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacion` (
  `IdFactura` int(11) NOT NULL,
  `producto` varchar(255) NOT NULL,
  `precio` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  KEY `factura` (`IdFactura`),
  CONSTRAINT `factura` FOREIGN KEY (`IdFactura`) REFERENCES `factura` (`IdFactura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacion`
--

LOCK TABLES `facturacion` WRITE;
/*!40000 ALTER TABLE `facturacion` DISABLE KEYS */;
INSERT INTO `facturacion` VALUES (4,'Pan Bimbo',4000,1,4000),(4,'Maiz Tierno',3000,3,9000),(4,'Gomitas Trululu',5000,1,5000),(1,'Queso',5000,1,5000),(1,'Pan Bimbo',4000,2,8000),(1,'Maiz Tierno',3000,1,3000),(2,'Gomitas Trululu',5000,1,5000),(2,'Maiz Tierno',3000,3,9000),(5,'Gomitas Trululu',5000,2,10000),(6,'Pan Bimbo',4000,1,4000),(7,'Pan Bimbo',4000,1,4000),(7,'Gomitas Trululu',5000,1,5000),(8,'Pan Bimbo',4000,1,4000),(8,'Gomitas Trululu',5000,1,5000),(9,'Maiz Tierno',3000,1,3000),(9,'Queso',5000,3,15000),(9,'Pan Bimbo',4000,1,4000),(10,'Gomitas Trululu',5000,2,10000),(10,'Pan Bimbo',4000,1,4000),(10,'Queso',5000,1,5000),(11,'Maiz Tierno',3000,1,3000),(11,'Queso',5000,2,10000),(12,'Maiz Tierno',3000,1,3000),(12,'Pan Bimbo',4000,3,12000),(12,'Queso',5000,1,5000),(13,'Nucita',3000,3,9000),(13,'Gomitas Trululu',5000,3,15000),(14,'Queso',5000,1,5000),(14,'Nucita',3000,1,3000),(15,'Uvas Rojas',5000,2,10000),(15,'Gomitas Trululu',5000,1,5000),(15,'Queso',5000,1,5000),(16,'Arroz',2500,1,2500),(16,'Gomitas Trululu',5000,1,5000),(16,'Uvas Rojas',5000,1,5000);
/*!40000 ALTER TABLE `facturacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `IdProducto` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `cantidad` varchar(20) DEFAULT NULL,
  `precio` decimal(20,2) DEFAULT NULL,
  `descripcion` text NOT NULL,
  `imagen` varchar(1000) NOT NULL,
  PRIMARY KEY (`IdProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (1,'Queso','220',5000.00,'Queso tajado x200g','1588527095_queso.jpg'),(2,'Gomitas Trululu','200',5000.00,'gomitas trululu x50 unidades','1588557089_gomitas.png'),(3,'Pan Bimbo','160',4000.00,'pan bimbo artesanal x300gr','1588558482_panbimbo.jpg'),(4,'Maiz Tierno','80',3000.00,'lata san jorge maiz tierno x300gr','1588559784_maiztierno.png'),(5,'Uvas Rojas','20',5000.00,'Uvas dulces ','1589901686_uvas.jpg'),(6,'Arroz','12',2500.00,'Arroz integral ','1589902600_arrozIntegral.jpg'),(21,'Nucita','3',3000.00,'Deliciosa crema de avellana ','1589815290_nucita.jpg');
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `IdProveedor` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `empresa` varchar(20) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `correo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`IdProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1000,'Mariza Gil','Alpina','31628','mari.gil@gmail.com'),(2000,'Alvaro Jimenez','Huggies','302','alv.jim@gmail.com'),(3000,'William Sanchez','Apple','315','willy.san@gmail.com'),(4000,'Sara','Adidas','399','sara@gmail.com'),(5000,'Lina Rios','Bimbo','3135','lr@gmail.com');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabajador`
--

DROP TABLE IF EXISTS `trabajador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trabajador` (
  `IdTrabajador` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `cargo` varchar(20) DEFAULT NULL,
  `salario` varchar(20) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  `direccion` varchar(20) DEFAULT NULL,
  `correo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`IdTrabajador`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabajador`
--

LOCK TABLES `trabajador` WRITE;
/*!40000 ALTER TABLE `trabajador` DISABLE KEYS */;
INSERT INTO `trabajador` VALUES (1,'Rodrigo Sanchez','Cajero','150000','321','calle 37','rod.san@gmail.com'),(2,'Patricia Maldonado','Domiciliario','550000','312','calle 27','pat.mal@gmail.com'),(3,'Daniel Flores','Cajero','750000','300','cra 15','dan.flo@gmail.com'),(4,'Maria','Domiciliario','2000','341','cra 14','mmp@gmail'),(5,'Andres Lopez','Cajero','700000','345','calle 10','anp@gmail.com');
/*!40000 ALTER TABLE `trabajador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `IdUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `correo` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  PRIMARY KEY (`IdUsuario`),
  UNIQUE KEY `correo` (`correo`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Luis Paez','lp@gmail.com','1234'),(8,'Maria Perez','mp@gmail.com','$2y$10$FZ6AaO7qjsd.w0drti/MFexpNL5L41F5xs9Yfib08vrRbF5D97vgi'),(9,'Rafael Diaz','rad@gmail.com','$2y$10$cCxDQL8C51FHQGevTDM6qun387YRLOlXMeLriv2Memtf10P9/.0HC'),(10,'Sofia Leon','sof@gmail.com','$2y$10$L0B.qX5JaVdx2z2oM1L5FuXV0f6ciV4ZaiJM856L.z.s8fWkcdIIy'),(11,'Ana Barrera','ab.12@gmail.com','$2y$10$Lz5Z.xUFWM5DOzTuuUOi1OYswWRQpNc4ad6ZuISPCWbbOva6SGgB6'),(12,'Ramon Osorio','ro64@gmail.com','$2y$10$LpxRqObvZkjek5QvNEhPZesuuUTD5ja5A0b0W0L0Ib2oTjM.s2ujG'),(14,'Fabian Morales','fb2@gmail.com','$2y$10$ZzSbruzGjzzCZj4tP/sfSeZiih7bAJB1VgXsDPjRFuQmm5FKFbyI.'),(15,'Isabella Marin','isam@gmail.com','$2y$10$DOcABfXrILRnhjd.G97yHOX4o0nyqnNQqRKpHCQKW8HLo2sISrx4O'),(16,'Alexander Montoya','alexm@gmail.com','$2y$10$upKzB2wz20YvvAxLJdiEkOYjXvaT8WX/VJqV9YbR0PxM3b.P/pE.u'),(17,'Angie Diaz','angieddd@gmail.com','$2y$10$vfLmje0mAep/TIKhHb28Le4nAYjbOt1Nc3.1ffc4Y8leut/NlBaNi'),(18,'Julian Flores','juliflo@gmail.com','$2y$10$p61DjYKCZtytTKARKiDmTubRAzHzspWHF/1f2J5DDoV/RNvBrVboW');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `venta`
--

DROP TABLE IF EXISTS `venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venta` (
  `IdUsuario` int(11) NOT NULL,
  `IdProducto` int(11) NOT NULL,
  KEY `usuario` (`IdUsuario`),
  KEY `producto` (`IdProducto`),
  CONSTRAINT `producto` FOREIGN KEY (`IdProducto`) REFERENCES `producto` (`IdProducto`),
  CONSTRAINT `usuario` FOREIGN KEY (`IdUsuario`) REFERENCES `usuario` (`IdUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `venta`
--

LOCK TABLES `venta` WRITE;
/*!40000 ALTER TABLE `venta` DISABLE KEYS */;
INSERT INTO `venta` VALUES (8,3),(8,3),(8,2),(8,3),(8,2),(10,4),(10,1),(10,3),(11,2),(11,3),(11,1),(12,4),(12,1),(14,4),(14,3),(14,1),(17,21),(17,2),(18,1),(18,21),(14,5),(14,2),(14,1),(14,6),(14,2),(14,5);
/*!40000 ALTER TABLE `venta` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-20 17:31:21
