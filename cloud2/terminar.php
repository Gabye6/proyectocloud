<?PHP
include("Conexion/config.php");
include("Conexion/conexion.php");
 include 'carrito.php';

 $txtTotal=0;
 foreach($_SESSION['CARRITO'] as $indice=>$principal){
     $id=$_SESSION['CARRITO'][$indice]['ID'];
     $sentencia=$pdo->prepare("SELECT*FROM producto
     WHERE IdProducto=:IdProducto");        
    $sentencia->bindParam(':IdProducto',$id);
    $sentencia->execute();
    $producto=$sentencia->fetch(PDO::FETCH_LAZY);

    //print_r($producto);

    $txtNombre=$producto['nombre'];
    $txtPrecio=$producto['precio'];
    $txtDescripcion=$producto['descripcion'];

    $newCantidad = $producto['cantidad']-$_SESSION['CARRITO'][$indice]['CANTIDAD'];

    //echo $newCantidad;

    $sentencia=$pdo->prepare("UPDATE producto SET 
    nombre=:nombre,
    cantidad=:cantidad,
    precio=:precio,
    descripcion=:descripcion WHERE IdProducto=:IdProducto");
    
    $sentencia->bindParam(':IdProducto',$id);
    $sentencia->bindParam(':nombre',$txtNombre);
    $sentencia->bindParam(':cantidad',$newCantidad);
    $sentencia->bindParam(':precio',$txtPrecio);
    $sentencia->bindParam(':descripcion',$txtDescripcion);
     $sentencia->execute();

     

 
   $txtTotal=$txtTotal+($principal['PRECIO']*$principal['CANTIDAD']);
   
 }
unset($_SESSION['CARRITO'])
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pago confirmado</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>

</head>
<body>
    <div class="container">
        <div class="jumbotron text-center">
            <h1 class="display-4">¡PAGO CONFIRMADO!</h1>
            <p class="lead">Total Pagado: <h4><?php echo "$".$txtTotal;?></h4></p>
            <p>Los productos seran enviados a la direccion ingresada</p>
            <hr class="my-4">
            <a class="btn btn-info btn-lg" href="tienda.php">VOLVER A LA TIENDA</a>
        </div>
    </div>
    
</body>
</html>
