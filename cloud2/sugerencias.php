<?php

include 'Conexion/config.php';
include 'Conexion/conexion.php';
include 'carrito.php';
include 'templates/cabecera.php';

?>

<br>
<h2 class="text-center"><?php echo "SUGERENCIAS PARA ".$_SESSION['USUARIO'][0]['NOMBRE']?></h2>

<div class="row">

<?php
    $sentencia=$pdo->prepare("SELECT count(IdProducto) as cantidad, IdProducto from venta group by IdProducto 
    order by cantidad desc");
    $sentencia-> execute();
    $listaProductos=$sentencia->fetchAll(PDO::FETCH_ASSOC);
   // print_r($listaProductos);

?>

<?php foreach($listaProductos as $principal){
    $id=$principal['IdProducto'];
    
    $sentencia=$pdo->prepare("SELECT * FROM producto
        WHERE IdProducto=:IdProducto");        
        $sentencia->bindParam(':IdProducto',$id);
        $sentencia->execute();
        $producto=$sentencia->fetch(PDO::FETCH_LAZY);
    
    
    ?>
    
    <div class="col-3">
        <div class="card">
            <img 

            title="<?php echo $producto['nombre'];?>"
             class="card-img-top" 
             src="Imagenes/<?php echo $producto['imagen'];?>"
             alt="<?php echo $producto['nombre'];?>"
             data-toggle="popover"
             data-content="<?php echo $producto['descripcion'];?>"
             data-trigger="hover"
             height="250px"
             
             >
            <div class="card-body">
                <span><?php echo $producto['nombre']?></span>
                <h5 class="card-title" >$<?php echo $producto['precio'];?></h5>
                

                <form action="" method="post">
                <input type="hidden" name="id" id="id" value="<?php echo openssl_encrypt($producto['IdProducto'],COD,KEY);?>">
                <input type="hidden" name="nombre" id="nombre" value="<?php echo openssl_encrypt($producto['nombre'],COD,KEY);?>">
                <input type="hidden" name="precio" id="precio" value="<?php echo openssl_encrypt($producto['precio'],COD,KEY);?>">
                <input type="hidden" name="cantidad" id="cantidad" value="<?php echo openssl_encrypt(1,COD,KEY);?>">    
                <button class="btn btn-primary" name="btnAccion" 
                    value="Agregar" type="submit" >Agregar al carrito</button>
                
                </form>

            </div>
        </div>

    </div>
<?php }?>

</div>


</div>

<?php
include 'templates/pie.php';
?>
