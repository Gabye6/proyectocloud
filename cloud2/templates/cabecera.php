<?php 

$accion=(isset($_POST['accion']))?$_POST['accion']:"";

switch($accion){
    case "btnCerrar";
    unset($_SESSION['USUARIO'][0]);
    header('Location: tienda.php');

    break;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>TIENDA</title>
    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
        
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="tienda.php">SUPER X</a>
        <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="my-nav" class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="tienda.php">HOME </a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link " href="mostrarCarrito.php">CARRITO(<?php
                    echo (empty($_SESSION['CARRITO']))?0:count($_SESSION['CARRITO']);
                    
                    ?>)</a>
                </li>

            </ul>

            <ul class="navbar-nav  navbar-right">
            <?php if(!empty($_SESSION['USUARIO'])){?>
                
                <li ><a href="sugerencias.php" class="nav-item active nav-link bg-dark">SUGERENCIAS</a></li>
                <li><a href="#" class="nav-item active nav-link"><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['USUARIO'][0]['NOMBRE'];?></a></li>
                
                <form action="" method="post">

                <button href="tienda.php" class="navbar-btn btn btn-danger" type="submit" name="accion" value="btnCerrar">CERRAR SESION</button>
                </form>
               
            </li>
                <?php }else{?>
                <li class="nav-item active">
                <a href="login/registro.php" class="nav-link bg-dark"><span class="glyphicon glyphicon-log-in "></span> REGISTRARSE</a>
                </li>
                <br>
                <li class="nav-item active dropdown ">
                <a class="dropdown-toggle nav-link" data-toggle="dropdown" href="#">INICIO SESION <span class="caret"></span></a>
                    <ul class="dropdown-menu bg-dark nav-dark">
                        <li ><a class="nav-item active nav-link" href="login/login.php">USUARIO</a></li>
                        <li ><a class="nav-item active nav-link" href="login/loginAdmin.php">ADMINISTRADOR</a></li>

                    </ul>
                </li>
                <?php }?>

            </ul>

        </div>
    </nav>
    <br/>
    <br/>
    <div class="container">