<?php
include 'Conexion/config.php';
include 'Conexion/conexion.php';
include 'carrito.php';
include 'templates/cabecera.php';

++$_SESSION['PAGINA'][0]['PAG'];
?>
<br>
<h3>Lista del carrito</h3>
<?php if(!empty($_SESSION['CARRITO'])){?>
<table class="table table-bordered">
    <tbody align="center">
        <tr >
            <th >Descripcion</th>
            <th  class="text-center">Cantidad</th>
            <th  class="text-center">Precio</th>
            <th  class="text-center">Total</th>
            <th >--</th>
        </tr>
        <?php $total= 0;?>
        <?php foreach($_SESSION['CARRITO'] as $indice=>$principal){?>
        
        <tr>
            <td ><?php echo $principal['NOMBRE']?></td>
            <td  class="text-center"><?php echo $principal['CANTIDAD'];?></td>
            <td class="text-center">$<?php echo $principal['PRECIO'];?></td>
            <td  class="text-center">$<?php echo number_format($principal['PRECIO']*$principal['CANTIDAD'],2) ;?></td>
            <td >
            <form action="" method="post">
                <input type="hidden" name="id" id="id" value="<?php echo openssl_encrypt($principal['ID'],COD,KEY);?>"> 
                <button 
                class="btn btn-success" 
                type="submit"
                name="btnAccion"
                value="Aumentar"
                >+</button>
                <input type="hidden" name="id" id="id" value="<?php echo openssl_encrypt($principal['ID'],COD,KEY);?>"> 
                <button 
                class="btn btn-warning" 
                type="submit"
                name="btnAccion"
                value="Disminuir"
                >-</button>
                <input type="hidden" name="id" id="id" value="<?php echo openssl_encrypt($principal['ID'],COD,KEY);?>"> 
                <button 
                class="btn btn-danger" 
                type="submit"
                name="btnAccion"
                value="Eliminar"
                >Eliminar</button>

            </form>    


            </td>
        </tr>
        <?php $total=$total+($principal['PRECIO']*$principal['CANTIDAD']);
        ?>
        <?php }?>

        <tr>
            <td colspan="3" align="right"><h3>Total</h3></td>
            <td align="right">$<?php echo number_format($total,2);?></td>
            <td></td>
        </tr>
        <tr>
            <?php if(!empty($_SESSION['USUARIO'])){
                ++$_SESSION['USUARIO'][0]['PAG'];?>
                
                <td colspan="5" ><a class="btn btn-info btn-lg btn-block" href="pago.php">Realizar Pago >></a></td>
            <?php }else{?>
                <td colspan="5" ><a class="btn btn-info btn-lg btn-block" href="login/login.php">Realizar Pago >></a></td>
            <?php }?>
         
        </tr>
    </tbody>
</table>
<?php } else{?>
    <div class="alert alert-success">
    No hay productos en el carrito
    </div>
    
<?php }?>
<?php
include 'templates/pie.php';
?>