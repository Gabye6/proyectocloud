<?php

include 'Conexion/config.php';
include 'Conexion/conexion.php';
include 'carrito.php';
include 'templates/cabecera.php';


$pag=array(
    'PAG'=>0
);
$_SESSION['PAGINA'][0]=$pag;

?>


        <br>
        <?php if($mensaje!=""){?>
        <div class="alert alert-success" >
        <?php echo $mensaje;?>
            <a href="mostrarCarrito.php" class="badge badge-success">Ver carrito</a>
        </div>
        <?php }?>

        <div class="row">

        <?php
            $sentencia=$pdo->prepare("SELECT * FROM producto");
            $sentencia-> execute();
            $listaProductos=$sentencia->fetchAll(PDO::FETCH_ASSOC);
            //print_r($listaProductos);
        
        ?>

        <?php foreach($listaProductos as $principal){?>
            <div class="col-3">
                <div class="card">
                    <img 

                    title="<?php echo $principal['nombre'];?>"
                     class="card-img-top" 
                     src="Imagenes/<?php echo $principal['imagen'];?>"
                     alt="<?php echo $principal['nombre'];?>"
                     data-toggle="popover"
                     data-content="<?php echo $principal['descripcion'];?>"
                     data-trigger="hover"
                     height="250px"
                     
                     >
                    <div class="card-body">
                        <span><?php echo $principal['nombre']?></span>
                        <h5 class="card-title" >$<?php echo $principal['precio'];?></h5>
                        

                        <form action="" method="post">
                        <input type="hidden" name="id" id="id" value="<?php echo openssl_encrypt($principal['IdProducto'],COD,KEY);?>">
                        <input type="hidden" name="nombre" id="nombre" value="<?php echo openssl_encrypt($principal['nombre'],COD,KEY);?>">
                        <input type="hidden" name="precio" id="precio" value="<?php echo openssl_encrypt($principal['precio'],COD,KEY);?>">
                        <input type="hidden" name="cantidad" id="cantidad" value="<?php echo openssl_encrypt(1,COD,KEY);?>">    
                        <button class="btn btn-primary" name="btnAccion" 
                            value="Agregar" type="submit" >Agregar al carrito</button>
                        
                        </form>

                    </div>
                </div>

            </div>
        <?php }?>

        </div>


    </div>
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
            })


    </script>
<?php
include 'templates/pie.php';
?>