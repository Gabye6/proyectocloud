<?php

include("../Conexion/config.php");
include("../Conexion/conexion.php");
?>

<?php
$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
$txtNombre=(isset($_POST['txtNombre']))?$_POST['txtNombre']:"";
$txtEmail=(isset($_POST['txtEmail']))?$_POST['txtEmail']:"";
$txtPassword=(isset($_POST['txtPassword']))?$_POST['txtPassword']:"";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$contraseña=password_hash($txtPassword,PASSWORD_BCRYPT);

$mensaje="";

switch($accion){
    case "btnRegistrar":
        $sentencia=$pdo->prepare("INSERT INTO usuario(nombre,correo,pass) VALUES (:nombre,:correo,:pass)");
       
             
        $sentencia->bindParam(':nombre',$txtNombre);
        $sentencia->bindParam(':correo',$txtEmail); 
        $sentencia->bindParam(':pass',$contraseña); 

        if($sentencia->execute()){
            $mensaje="Registro Exitoso. Ingrese a la cuenta haciendo click en LOGIN";
        }else{
            $mensaje="Error en el registro";
        }

        //$listatrabajador=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        //print_r($listatrabajador);

       
    
    break;
    case "btnLogin":
        header('Location: login.php');
       
    
    break;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
<a class="navbar-brand" href="tienda.php">REGISTRO</a>
       
        <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="my-nav" class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">

            </ul>

            <ul class="navbar-nav  navbar-right">

                <li>
                <a class="nav-link nav-item active " href="../tienda.php"><< VOLVER</a>


                </li>
           


            </ul>

        </div>
    </nav>
    <br>
    <br>
    <br>

    <?php if($mensaje!=""){?>
        <div class="alert alert-success" >
        <?php echo $mensaje;?>
        </div>
        <?php }?>
    <div class="container">
        <form action="" method="post">
        <div class="card mx-auto " style="width: 25rem;  " >
                <div class="card-header text-center">
                <h2>REGISTRO</h2>
                </div>
                <div class="card-body">

                    <input type="hidden" name="txtID"  placeholder="" class="form-control" id="txtID">

                    <div class="form-group">
                    <label for="">Nombre:</label>
                    <input type="text" name="txtNombre"  placeholder="" class="form-control" id="txtNombre">
                    </div>

                    <div class="form-group">
                    <label for="">Email:</label>
                    <input type="email" name="txtEmail"  placeholder="" class="form-control" id="txtEmail">
                    </div>

                    <div class="form-group">
                    <label for="">Password:</label>
                    <input type="password" name="txtPassword"  placeholder="" class="form-control" id="txtPassword">
                    </div>


                </div>
                <div class="card-footer text-center">
                <button value="btnRegistrar" type="submit" name="accion" class="btn btn-info btn-lg btn-block">Registrarse</button>
                <hr>
                <button value="btnLogin" type="submit" name="accion" class="btn btn-success btn-lg btn-block ">Login</button>
                
                </div>
            </div>

        </form>
    </div>
    
</body>
</html>