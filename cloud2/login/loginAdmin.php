<?php
include 'codAdmin.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
</head>
<body>
    <br>
    <br>
    <div class="container">
    <?php if($mensaje!=""){?>
        <div class="alert alert-danger" >
        <?php echo $mensaje;?>
        </div>
        <?php }?>

        <form action="" method="post">
            <div class="card mx-auto " style="width: 25rem;  " >
                <div class="card-header text-center">
                <h2>LOGIN</h2>
                </div>
                <div class="card-body">

                    <div class="form-group">
                    <label for="">Email:</label>
                    <input type="text" name="txtEmail"  placeholder="Ingrese email" class="form-control" id="txtEmail">
                    </div>

                    <div class="form-group">
                    <label for="">Password:</label>
                    <input type="password" name="txtPassword"  placeholder="Ingrese password" class="form-control" id="txtPassword">
                    </div>


                </div>
                <div class="card-footer text-center">
                <button value="btnIngresar" type="submit" name="accion" class="btn btn-success btn-lg btn-block ">Ingresar</button>
                <hr>
                <button value="btnRegistro" type="submit" name="accion" class="btn btn-info btn-lg btn-block ">Registrar</button>
                </div>
            </div>



        </form>
    </div>
    
</body>
</html>