<?php 
$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
$txtNombre=(isset($_POST['txtNombre']))?$_POST['txtNombre']:"";
$txtCantidad=(isset($_POST['txtCantidad']))?$_POST['txtCantidad']:"";
$txtPrecio=(isset($_POST['txtPrecio']))?$_POST['txtPrecio']:"";
$txtDescripcion=(isset($_POST['txtDescripcion']))?$_POST['txtDescripcion']:"";
$txtImagen=(isset($_FILES['txtImagen']["name"]))?$_FILES['txtImagen']["name"]:"";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";


$mostrarModal=false;



switch($accion){

    case "btnAgregar":

        $sentencia=$pdo->prepare("INSERT INTO producto(IdProducto,nombre,cantidad,precio,descripcion,imagen) 
        VALUES (:IdProducto,:nombre,:cantidad,:precio,:descripcion,:imagen) ");

        $sentencia->bindParam(':IdProducto',$txtID);
        $sentencia->bindParam(':nombre',$txtNombre);
        $sentencia->bindParam(':cantidad',$txtCantidad);
        $sentencia->bindParam(':precio',$txtPrecio);
        $sentencia->bindParam(':descripcion',$txtDescripcion);
        $Fecha= new DateTime();
        $nombreArchivo=($txtImagen!="")?$Fecha->getTimestamp()."_".$_FILES["txtImagen"]["name"]:"imagen.jpg";

        $tmpImagen=$_FILES["txtImagen"]["tmp_name"];
        if($tmpImagen!=""){
            move_uploaded_file($tmpImagen,"../Imagenes/".$nombreArchivo);
        }

            
        $sentencia->bindParam(':imagen',$nombreArchivo);
        
        $sentencia->execute();

        $txtID="";
        $txtNombre="";
        $txtCantidad="";
        $txtPrecio="";
        $txtDescripcion="";
        $txtImagen="";

        header('Location: producto.php');

    break;
    case "btnModificar":

        $sentencia=$pdo->prepare("UPDATE producto SET 
        nombre=:nombre,
        cantidad=:cantidad,
        precio=:precio,
        descripcion=:descripcion WHERE IdProducto=:IdProducto");
        
        $sentencia->bindParam(':IdProducto',$txtID);
        $sentencia->bindParam(':nombre',$txtNombre);
        $sentencia->bindParam(':cantidad',$txtCantidad);
        $sentencia->bindParam(':precio',$txtPrecio);
        $sentencia->bindParam(':descripcion',$txtDescripcion);

        $sentencia->execute();

        $Fecha= new DateTime();
        $nombreArchivo=($txtImagen!="")?$Fecha->getTimestamp()."_".$_FILES["txtImagen"]["name"]:"imagen.jpg";

        $tmpImagen=$_FILES["txtImagen"]["tmp_name"];
        
        if($tmpImagen!=""){
            move_uploaded_file($tmpImagen,"../Imagenes/".$nombreArchivo);
            
            $sentencia=$pdo->prepare("SELECT imagen FROM producto
            WHERE IdProducto=:IdProducto");  
            $sentencia->bindParam(':IdProducto',$txtID);
            $sentencia->execute();
            $producto=$sentencia->fetch(PDO::FETCH_LAZY);
            if(isset($producto['imagen'])){
                if(file_exists("../Imagenes/".$producto["imagen"])){
                    if($producto['imagen']!="imagen.jpg"){
                        unlink("../Imagenes/".$producto["imagen"]);
                    }
                }
    
            }
            
            
            
            $sentencia=$pdo->prepare("UPDATE producto SET 
            imagen=:imagen WHERE IdProducto=:IdProducto");
    
            $sentencia->bindParam(':imagen',$nombreArchivo);
            $sentencia->bindParam(':IdProducto',$txtID);
            $sentencia->execute();
        
        }
        $txtID="";
        $txtNombre="";
        $txtCantidad="";
        $txtPrecio="";
        $txtDescripcion="";
        $txtImagen="";


        header('Location: producto.php');
        
    break;
    case "btnEliminar":
        $sentencia=$pdo->prepare("SELECT imagen FROM producto
         WHERE IdProducto=:IdProducto");  
        $sentencia->bindParam(':IdProducto',$txtID);
        $sentencia->execute();
        $producto=$sentencia->fetch(PDO::FETCH_LAZY);
        if(isset($producto['imagen'])&&$producto['imagen']!="imagen.jpg"){
            if(file_exists("../Imagenes/".$producto["imagen"])){
             
                    unlink("../Imagenes/".$producto["imagen"]);
                
            }

        }
         
         
       $sentencia=$pdo->prepare("DELETE FROM producto
         WHERE IdProducto=:IdProducto");        
        $sentencia->bindParam(':IdProducto',$txtID);
        $sentencia->execute();

        $txtID="";
        $txtNombre="";
        $txtCantidad="";
        $txtPrecio="";
        $txtDescripcion="";
        $txtImagen="";

        header('Location: producto.php');

    break;
    case "btnBuscar":
        $mostrarModal=true;
        $sentencia=$pdo->prepare("SELECT * FROM producto
        WHERE IdProducto=:IdProducto");        
        $sentencia->bindParam(':IdProducto',$txtID);
        $sentencia->execute();
        $producto=$sentencia->fetch(PDO::FETCH_LAZY);

        $txtNombre=$producto['nombre'];
        $txtCantidad=$producto['cantidad'];
        $txtPrecio=$producto['precio'];
        $txtDescripcion=$producto['descripcion'];
        $txtImagen=$producto['imagen'];
        

    break;
    case "Seleccionar":

        $mostrarModal=true;

        $sentencia=$pdo->prepare("SELECT * FROM producto
        WHERE IdProducto=:IdProducto");        
        $sentencia->bindParam(':IdProducto',$txtID);
        $sentencia->execute();
        $producto=$sentencia->fetch(PDO::FETCH_LAZY);

        $txtNombre=$producto['nombre'];
        $txtCantidad=$producto['cantidad'];
        $txtPrecio=$producto['precio'];
        $txtDescripcion=$producto['descripcion'];
        $txtImagen=$producto['imagen'];
    break;

    case "btnCancelar":
        header('Location: producto.php');
        $txtID="";
        $txtNombre="";
        $txtCantidad="";
        $txtPrecio="";
        $txtDescripcion="";
        $txtImagen="";
    break;

    case "btnCerrar";
    unset($_SESSION['USUARIO'][0]);
    header('Location: ../tienda.php');

    break;
    case "btnVolver";
  
    header('Location: Admin.php');

    break;
}
        $sentencia=$pdo->prepare("SELECT * FROM producto ORDER BY cantidad asc");        
        $sentencia->execute();
        $listaproducto=$sentencia->fetchAll(PDO::FETCH_ASSOC);

?>