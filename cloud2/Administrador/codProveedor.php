<?php 

//TRABAJADOR

$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
$txtNombre=(isset($_POST['txtNombre']))?$_POST['txtNombre']:"";
$txtEmpresa=(isset($_POST['txtEmpresa']))?$_POST['txtEmpresa']:"";
$txtTelefono=(isset($_POST['txtTelefono']))?$_POST['txtTelefono']:"";
$txtCorreo=(isset($_POST['txtCorreo']))?$_POST['txtCorreo']:"";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$mostrarModal=false;



switch($accion){

    case "btnAgregar":

        $sentencia=$pdo->prepare("INSERT INTO proveedor(IdProveedor,nombre,empresa,telefono,correo) 
        VALUES (:IdProveedor,:nombre,:empresa,:telefono,:correo) ");
       
       $sentencia->bindParam(':IdProveedor',$txtID);
       $sentencia->bindParam(':nombre',$txtNombre);
       $sentencia->bindParam(':empresa',$txtEmpresa);
       $sentencia->bindParam(':telefono',$txtTelefono);
       $sentencia->bindParam(':correo',$txtCorreo);
        $sentencia->execute();

        $txtID="";
        $txtNombre="";
        $txtEmpresa="";
        $txtTelefono="";
        $txtCorreo="";

        header('Location: proveedor.php');

    break;
    case "btnModificar":

        $sentencia=$pdo->prepare("UPDATE proveedor SET 
        nombre=:nombre,
        empresa=:empresa,
        telefono=:telefono,
        correo=:correo WHERE IdProveedor=:IdProveedor");
        
        $sentencia->bindParam(':IdProveedor',$txtID);
        $sentencia->bindParam(':nombre',$txtNombre);
        $sentencia->bindParam(':empresa',$txtEmpresa);
        $sentencia->bindParam(':telefono',$txtTelefono);
        $sentencia->bindParam(':correo',$txtCorreo);
        $sentencia->execute();

        $txtID="";
        $txtNombre="";
        $txtEmpresa="";
        $txtTelefono="";
        $txtCorreo="";


        header('Location: proveedor.php');
        
    break;
    case "btnEliminar":
        $sentencia=$pdo->prepare("DELETE FROM proveedor
         WHERE IdProveedor=:IdProveedor");        
        $sentencia->bindParam(':IdProveedor',$txtID);
        $sentencia->execute();

        $txtID="";
        $txtNombre="";
        $txtEmpresa="";
        $txtTelefono="";
        $txtCorreo="";


        header('Location: proveedor.php');
    break;
    case "btnBuscar":
  
        $sentencia=$pdo->prepare("SELECT * FROM proveedor
        WHERE IdProveedor=:IdProveedor");        
        $sentencia->bindParam(':IdProveedor',$txtID);
        $sentencia->execute();
        $proveedor=$sentencia->fetch(PDO::FETCH_LAZY);

        $txtNombre=$proveedor['nombre'];
        $txtEmpresa=$proveedor['empresa'];
        $txtTelefono=$proveedor['telefono'];
        $txtCorreo=$proveedor['correo'];

        $mostrarModal=true;
    break;
    case "Seleccionar":

        $mostrarModal=true;

        $sentencia=$pdo->prepare("SELECT * FROM proveedor
        WHERE IdProveedor=:IdProveedor");        
        $sentencia->bindParam(':IdProveedor',$txtID);
        $sentencia->execute();
        $proveedor=$sentencia->fetch(PDO::FETCH_LAZY);

        $txtNombre=$proveedor['nombre'];
        $txtEmpresa=$proveedor['empresa'];
        $txtTelefono=$proveedor['telefono'];
        $txtCorreo=$proveedor['correo'];
    break;
    case "btnCancelar":

        $txtID="";
        $txtNombre="";
        $txtEmpresa="";
        $txtTelefono="";
        $txtCorreo="";

        header('Location: proveedor.php');
    break;
    case "btnCerrar";
    unset($_SESSION['USUARIO'][0]);
    header('Location: ../tienda.php');

    break;
    case "btnVolver";
  
    header('Location: Admin.php');

    break;

}
$sentencia=$pdo->prepare("SELECT * FROM proveedor");        
$sentencia->execute();
$listaproveedor=$sentencia->fetchAll(PDO::FETCH_ASSOC);


?>