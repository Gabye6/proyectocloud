<?php
include '../login/codAdmin.php';
require 'codProducto.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PRODUCTO CRUD</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

       
<button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div id="my-nav" class="collapse navbar-collapse">
    <ul class="navbar-nav mr-auto">
        <li>
        <form action="" method="post">

        <button href="tienda.php" class="navbar-btn btn btn-info" type="submit" name="accion" value="btnVolver">VOLVER AL MENU</button>
        </form>

        </li>



    </ul>

    <ul class="navbar-nav  navbar-right">

        <li class="nav-item active">
        <li><a href="#" class="nav-link nave-item active" ><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['USUARIO'][0]['NOMBRE'];?></a></li>
        </li>
        <form action="" method="post">

        <button href="tienda.php" class="navbar-btn btn btn-danger" type="submit" name="accion" value="btnCerrar">CERRAR SESION</button>
        </form>
       
        </li>


    </ul>

</div>
</nav>
    <div class="container">

        <form action="" method="post" enctype="multipart/form-data">



           <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Producto</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="">ID:</label>
                                <input type="text" class="form-control"  name="txtID" value="<?php echo $txtID?>" placeholder="" id="txtID" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-6">
                                <label for="">Nombre:</label>
                                <input type="text" class="form-control" name="txtNombre" value="<?php echo $txtNombre?>" placeholder="" id="txtNombre" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-6">
                                <label for="">Cantidad:</label>
                                <input type="text" class="form-control" name="txtCantidad" value="<?php echo $txtCantidad?>" placeholder="" id="txtCantidad" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-6">
                                <label for="">Precio:</label>
                                <input type="text" class="form-control" name="txtPrecio" value="<?php echo $txtPrecio?>" placeholder="" id="txtPrecio" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-12">
                                <label for="">Descripcion:</label>
                                <input type="text" class="form-control" name="txtDescripcion" value="<?php echo $txtDescripcion?>" placeholder="" id="txtDescripcion" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-12">
                                <label for="">Imagen:</label>
                                <?php if($txtImagen!=""){?>
                                    <br/>
                                    <img class="img-thumbnail rounded mx-auto d-block" width="100px"
                                    src="../Imagenes/<?php echo $txtImagen;?>"/>
                                    <br/>
                                    <br/>

                                <?php }?>
                                <input type="file" class="form-control" accept="image/*" name="txtImagen" value="<?php echo $txtImagen?>" placeholder="" id="txtImagen" require="">
                                <br>
                                </div>

                           </div>
                        </div>
                        <div class="modal-footer">
                            
                            <button value="btnAgregar"  class ="btn btn-success" type="submit" name="accion">AGREGAR</button>
                            <button value="btnModificar" class ="btn btn-warning" type="submit" name="accion">MODIFICAR</button>
                            <button value="btnEliminar" onclick="return Confirmar('¿Desea borrar?');" class ="btn btn-danger" type="submit" name="accion">ELIMINAR</button>
                            <button value="btnBuscar"  class ="btn btn-info" type="submit" name="accion">BUSCAR</button>
                            <button value="btnCancelar"  class ="btn btn-primary"  type="submit" name="accion">CANCELAR</button>

                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- Button trigger modal -->
            <br/>
            <br/>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
            Configuracion
            </button>

            <br/>
            <br/>
        



            
        </form>

        <div class="row">
        <table class="table table-hover table-bordered">
            <thead class="thead-dark" align="center">
                <tr>
                    <th>Imagen</th>
                    <th>Nombre</th>
                    <th>Cantidad</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <?php foreach($listaproducto as $producto){
                if($producto['cantidad']<20){?>
                <tr class="table-danger" align="center">
                    <td><img class="img-thumbnail" width="100px" src="../Imagenes/<?php echo $producto['imagen'];?>"/></td>
                    <td><?php echo $producto['nombre'];?></td>
                    <td><?php echo $producto['cantidad'];?></td>
                    <td>
                        <form action="" method="post">

                            <input type="hidden" name="txtID" value="<?php echo $producto['IdProducto']?>">                        

                            <input type="submit"  class ="btn btn-info" value="Seleccionar" name ="accion">
                            <button value="btnEliminar" onclick="return Confirmar('¿Desea borrar?');" class ="btn btn-danger" type="submit" name="accion">ELIMINAR</button>
                        
                        </form>
                    </td>
                    
                    
                </tr>
                <?php }else{?>
                    <tr align="center">
                    <td><img class="img-thumbnail" width="100px" src="../Imagenes/<?php echo $producto['imagen'];?>"/></td>
                    <td><?php echo $producto['nombre'];?></td>
                    <td><?php echo $producto['cantidad'];?></td>
                    <td>
                        <form action="" method="post">

                            <input type="hidden" name="txtID" value="<?php echo $producto['IdProducto']?>">                        

                            <input type="submit"  class ="btn btn-info" value="Seleccionar" name ="accion">
                            <button value="btnEliminar" onclick="return Confirmar('¿Desea borrar?');" class ="btn btn-danger" type="submit" name="accion">ELIMINAR</button>
                        
                        </form>
                    </td>
                    
                    
                </tr>



                <?php }?>



            <?php }?>
        </table>
   
    
 
    </div>
<?php if($mostrarModal){?>
    <script>
        $('#exampleModal').modal('show');
    </script>
<?php }?>
<script>
    function Confirmar(Mensaje){
        return (confirm(Mensaje))?true:false;
    }
</script>

        
    </div>
</body>
</html>