<?php 


$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
$txtNombre=(isset($_POST['txtNombre']))?$_POST['txtNombre']:"";
$txtCargo=(isset($_POST['txtCargo']))?$_POST['txtCargo']:"";
$txtSalario=(isset($_POST['txtSalario']))?$_POST['txtSalario']:"";
$txtTelefono=(isset($_POST['txtTelefono']))?$_POST['txtTelefono']:"";
$txtDireccion=(isset($_POST['txtDireccion']))?$_POST['txtDireccion']:"";
$txtCorreo=(isset($_POST['txtCorreo']))?$_POST['txtCorreo']:"";


$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$mostrarModal=false;



switch($accion){

    case "btnAgregar":

        $sentencia=$pdo->prepare("INSERT INTO trabajador(IdTrabajador,nombre,cargo,salario,telefono,direccion,correo) 
        VALUES (:IdTrabajador,:nombre,:cargo,:salario,:telefono,:direccion,:correo) ");
       
        $sentencia->bindParam(':IdTrabajador',$txtID);
        $sentencia->bindParam(':nombre',$txtNombre);
        $sentencia->bindParam(':cargo',$txtCargo);
        $sentencia->bindParam(':salario',$txtSalario);
        $sentencia->bindParam(':telefono',$txtTelefono);
        $sentencia->bindParam(':direccion',$txtDireccion);
        $sentencia->bindParam(':correo',$txtCorreo);
        $sentencia->execute();


        header('Location: trabajador.php');

    break;
    case "btnModificar":

        $sentencia=$pdo->prepare("UPDATE trabajador SET 
        nombre=:nombre,
        cargo=:cargo,
        salario=:salario,
        telefono=:telefono,
        direccion=:direccion,
        correo=:correo WHERE IdTrabajador=:IdTrabajador");
        
        $sentencia->bindParam(':IdTrabajador',$txtID);
        $sentencia->bindParam(':nombre',$txtNombre);
        $sentencia->bindParam(':cargo',$txtCargo);
        $sentencia->bindParam(':salario',$txtSalario);
        $sentencia->bindParam(':telefono',$txtTelefono);
        $sentencia->bindParam(':direccion',$txtDireccion);
        $sentencia->bindParam(':correo',$txtCorreo);
        $sentencia->execute();

        $txtNombre="";
        $txtCargo="";
        $txtSalario="";
        $txtTelefono="";
        $txtDireccion="";
        $txtCorreo="";

        header('Location: trabajador.php');
        
    break;
    case "btnEliminar":
        $sentencia=$pdo->prepare("DELETE FROM trabajador
         WHERE IdTrabajador=:IdTrabajador");        
        $sentencia->bindParam(':IdTrabajador',$txtID);
        $sentencia->execute();

        $txtNombre="";
        $txtCargo="";
        $txtSalario="";
        $txtTelefono="";
        $txtDireccion="";
        $txtCorreo="";

        header('Location: trabajador.php');
    break;
    case "btnBuscar":
        $mostrarModal=true;
        $sentencia=$pdo->prepare("SELECT * FROM trabajador
        WHERE IdTrabajador=:IdTrabajador");        
        $sentencia->bindParam(':IdTrabajador',$txtID);
        $sentencia->execute();
        $trabajador=$sentencia->fetch(PDO::FETCH_LAZY);

        $txtNombre=$trabajador['nombre'];
        $txtCargo=$trabajador['cargo'];
        $txtSalario=$trabajador['salario'];
        $txtTelefono=$trabajador['telefono'];
        $txtDireccion=$trabajador['direccion'];
        $txtCorreo=$trabajador['correo'];

    break;
    case "Seleccionar":

        $mostrarModal=true;

        $sentencia=$pdo->prepare("SELECT * FROM trabajador
        WHERE IdTrabajador=:IdTrabajador");        
        $sentencia->bindParam(':IdTrabajador',$txtID);
        $sentencia->execute();
        $trabajador=$sentencia->fetch(PDO::FETCH_LAZY);

        $txtID=$trabajador['IdTrabajador'];
        $txtNombre=$trabajador['nombre'];
        $txtCargo=$trabajador['cargo'];
        $txtSalario=$trabajador['salario'];
        $txtTelefono=$trabajador['telefono'];
        $txtDireccion=$trabajador['direccion'];
        $txtCorreo=$trabajador['correo'];
    break;
    case "btnCancelar":
        $txtNombre="";
        $txtCargo="";
        $txtSalario="";
        $txtTelefono="";
        $txtDireccion="";
        $txtCorreo="";

        header('Location: trabajador.php');
    break;
    case "btnCerrar";
    unset($_SESSION['USUARIO'][0]);
    header('Location: ../tienda.php');

    break;
    case "btnVolver";
  
    header('Location: Admin.php');

    break;

}
$sentencia=$pdo->prepare("SELECT * FROM trabajador");        
$sentencia->execute();
$listatrabajador=$sentencia->fetchAll(PDO::FETCH_ASSOC);
?>