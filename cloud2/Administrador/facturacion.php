<?php

include '../login/codAdmin.php';

?>

<?php
$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$mostrarModal=false;

$sentencia=$pdo->prepare("SELECT * FROM factura");        
$sentencia->execute();
$listafactura=$sentencia->fetchAll(PDO::FETCH_ASSOC);
//print_r($listafactura);
switch($accion){
    case "productos":

        echo $txtID;
        $sentencia=$pdo->prepare("SELECT * from facturacion where IdFactura=:IdFactura");   
        $sentencia->bindParam(':IdFactura',$txtID); 
        $sentencia->execute();
        $listaproducto=$sentencia->fetchAll(PDO::FETCH_ASSOC);
        //print_r($listaproducto);

        $mostrarModal=true;
    break;
    case "btnCerrar";
    unset($_SESSION['USUARIO'][0]);
    header('Location: ../tienda.php');

    break;
    case "btnVolver";
  
    header('Location: Admin.php');

    break;

}




?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Facturacion</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

       
<button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div id="my-nav" class="collapse navbar-collapse">
    <ul class="navbar-nav mr-auto">
        <li>
        <form action="" method="post">

        <button href="tienda.php" class="navbar-btn btn btn-info" type="submit" name="accion" value="btnVolver">VOLVER AL MENU</button>
        </form>

        </li>



    </ul>

    <ul class="navbar-nav  navbar-right">

        <li class="nav-item active">
        <li><a href="#" class="nav-link nave-item active" ><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['USUARIO'][0]['NOMBRE'];?></a></li>
        </li>
        <form action="" method="post">

        <button href="tienda.php" class="navbar-btn btn btn-danger" type="submit" name="accion" value="btnCerrar">CERRAR SESION</button>
        </form>
       
        </li>


    </ul>

</div>
</nav>
<br>
<br>
<br>
<br>
    <div class="container">
        <div class="row">
            
            <table class="table table-hover table-bordered">
                <thead class="thead-dark" align="center">
                    <tr>
                        <th>ID</th>
                        <th>Titular</th>
                        <th>Direccion</th>
                        <th>Fecha</th>
                        <th>Total Factura</th>
                        <th>Productos</th>
                    </tr>
                </thead>
                <?php foreach($listafactura as $factura){?>
                    <tr align="center">
                        <td><?php echo $factura['IdFactura'];?></td>
                        <td><?php echo $factura['titular'];?></td>
                        <td><?php echo $factura['direccion'];?></td>
                        <td><?php echo $factura['fecha'];?></td>
                        <td><?php echo $factura['total'];?></td>
                        <td>
                            <form action="" method="post">
                                <!-- Modal Buscar-->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Productos</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="form-row">
                                                    <table class="table table-hover table-bordered text-center" >
                                                        <thead>
                                                            <tr>
                                                                <th>Nombre</th>
                                                                <th>Cantidad</th>
                                                                <th>Precio</th>
                                                                <th>Total</th>
                                                            </tr>
                                                        </thead>
                                                        <?php foreach($listaproducto as $producto){?>
                                                        <tr>

                                                            <form action="" method="post">
                                                            <td><?php echo $producto['producto'];?></td>
                                                            <td><?php echo $producto['cantidad'];?></td>
                                                            <td><?php echo $producto['precio'];?></td>
                                                            <td><?php echo $producto['total'];?></td>

                                                            </form>

                                                        </tr>
                                                        <?php }?>


                                                    </table>

                                            </div>
                                            </div>
                                            <div class="modal-footer">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Button trigger modal //-->
                                
                                <input type="hidden" name="txtID" value="<?php echo $factura['IdFactura'];?>"> 

                                
                                <button type="submit" value="productos" name="accion"  class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
                                Productos
                                </button>
                    

                     

                               
                            </form>
                        </td>
                        
                        
                    </tr>



                <?php }?>
            </table>

        </div>



<?php if($mostrarModal){?>
    <script>
        $('#exampleModal').modal('show');
    </script>
<?php }?>

    </div>


    
</body>
</html>