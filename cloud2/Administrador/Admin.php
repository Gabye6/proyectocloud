<?php
include '../login/codAdmin.php';
?>
<?php 
$accion=(isset($_POST['accion']))?$_POST['accion']:"";

switch($accion){
    case "btnCerrar";
    unset($_SESSION['USUARIO'][0]);
    header('Location: ../tienda.php');

    break;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>

    <style> 


        .div2 {
        width: 950px;
        height: 500px;  
        padding: 50px;
        border: 1px solid black;
        text-align: center;
        }

        div.gallery {
        margin: 15px;
        border: 1px solid #ccc;
        float: left;
        width: 180px;
        
        }

        div.gallery:hover {
        border: 1px solid #777;
        }

        div.gallery img {
        width: 100%;
        height: 200px;
        
        }

        div.desc {
        padding: 15px;
        text-align: center;
        }
</style>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
<a class="navbar-brand" href="tienda.php">ADMINISTRADOR</a>
       
        <button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div id="my-nav" class="collapse navbar-collapse">
            <ul class="navbar-nav mr-auto">

            </ul>

            <ul class="navbar-nav  navbar-right">

                <li class="nav-item active">
                <li><a href="#" class="nav-link nave-item active" ><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['USUARIO'][0]['NOMBRE'];?></a></li>
                </li>
                <form action="" method="post">

                <button href="tienda.php" class="navbar-btn btn btn-danger" type="submit" name="accion" value="btnCerrar">CERRAR SESION</button>
                </form>
               
                </li>


            </ul>

        </div>
    </nav>
    <br>
    <br>
    <div class="container" align="center">
        <br>
        <br>


        <div class="div2" >
            <h1 align="center">ADMINISTRADOR</h1>
            <div class="gallery">
                <a target="_blank">
                    <img src="../Imagenes/trabajador.jpg" alt="Trabajador" width="600" height="400">
                </a>
                <div class="desc">
                    <a class="btn btn-info"href="trabajador.php">Control Trabajador</a>
                </div>
            </div>

            <div class="gallery">
                <a target="_blank">
                    <img src="../Imagenes/inventario.jpg" alt="Inventario" width="600" height="400">
                </a>
                <div class="desc">
                    <a class="btn btn-info"href="producto.php">Control Ineventario</a>
                </div>
            </div>

            <div class="gallery">
                <a target="_blank">
                    <img src="../Imagenes/proveedor.jpg" alt="Proveedor" width="600" height="400">
                </a>
                <div class="desc">
                    <a class="btn btn-info"href="proveedor.php">Control Proveedor</a>
                </div>
            </div>

            <div class="gallery">
                <a target="_blank">
                    <img src="../Imagenes/pesos.png" alt="Proveedor" width="600" height="400">
                </a>
                <div class="desc">
                    <a class="btn btn-info"href="facturacion.php">Revisar Facturacion</a>
                </div>
            </div>

        </div>


    </div>

</body>
</html>