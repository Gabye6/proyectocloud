<?php
include '../login/codAdmin.php';
require 'codProveedor.php'
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PRODUCTO CRUD</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">

       
<button class="navbar-toggler" data-target="#my-nav" data-toggle="collapse" aria-controls="my-nav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div id="my-nav" class="collapse navbar-collapse">
    <ul class="navbar-nav mr-auto">
        <li>
        <form action="" method="post">

        <button href="tienda.php" class="navbar-btn btn btn-info" type="submit" name="accion" value="btnVolver">VOLVER AL MENU</button>
        </form>

        </li>



    </ul>

    <ul class="navbar-nav  navbar-right">

        <li class="nav-item active">
        <li><a href="#" class="nav-link nave-item active" ><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION['USUARIO'][0]['NOMBRE'];?></a></li>
        </li>
        <form action="" method="post">

        <button href="tienda.php" class="navbar-btn btn btn-danger" type="submit" name="accion" value="btnCerrar">CERRAR SESION</button>
        </form>
       
        </li>


    </ul>

</div>
</nav>
    <div class="container">

        <form action="" method="post" enctype="multipart/form-data">


            <!-- Modal Buscar-->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Proveedor</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           <div class="form-row">
                                <div class="form-group col-md-6">
                                <label for="">ID:</label>
                                <input type="text" class="form-control"  name="txtID" value="<?php echo $txtID?>" placeholder="" id="txtID" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-6">
                                <label for="">Nombre:</label>
                                <input type="text" class="form-control" name="txtNombre" value="<?php echo $txtNombre?>" placeholder="" id="txtNombre" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-6">
                                <label for="">Empresa:</label>
                                <input type="text" class="form-control" name="txtEmpresa" value="<?php echo $txtEmpresa?>" placeholder="" id="txtEmpresa" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-6">
                                <label for="">Telefono:</label>
                                <input type="text" class="form-control" name="txtTelefono" value="<?php echo $txtTelefono?>" placeholder="" id="txtTelefono" require="">
                                <br>
                                </div>

                                <div class="form-group col-md-12">
                                <label for="">Correo:</label>
                                <input type="text" class="form-control" name="txtCorreo" value="<?php echo $txtCorreo?>" placeholder="" id="txtCorreo" require="">
                                <br>
                                </div>

                           </div>
                        </div>
                        <div class="modal-footer">

                            <button value="btnAgregar"  class ="btn btn-success" type="submit" name="accion">AGREGAR</button>
                            <button value="btnModificar" class ="btn btn-warning" type="submit" name="accion">MODIFICAR</button>
                            <button value="btnEliminar" onclik="return Confirmar('¿Desea borrar?');" class ="btn btn-danger" type="submit" name="accion">ELIMINAR</button>
                            <button value="btnBuscar"  class ="btn btn-info" type="submit" name="accion">BUSCAR</button>
                            <button value="btnCancelar"  class ="btn btn-primary"  type="submit" name="accion">CANCELAR</button>

                        
                        </div>
                    </div>
                </div>
            </div>
            <!-- Button trigger modal -->
            <br/>
            <br/>
            <br/>
            <br/>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal">
            Configuracion
            </button>

            <br/>
            <br/>
        



            
        </form>

        <div class="row">
        <table class="table table-hover table-bordered">
            <thead class="thead-dark" align="center">
                <tr>
                    <th>ID</th>
                    <th>Nombre</th>
                    <th>Empresa</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <?php foreach($listaproveedor as $proveedor){?>
                <tr align="center">
                    <td><?php echo $proveedor['IdProveedor'];?></td>
                    <td><?php echo $proveedor['nombre'];?></td>
                    <td><?php echo $proveedor['empresa'];?></td>
                    <td>
                        <form action="" method="post">

                            <input type="hidden" name="txtID" value="<?php echo $proveedor['IdProveedor']?>">                        

                            <input type="submit"  class ="btn btn-info" value="Seleccionar" name ="accion">
                            <button value="btnEliminar" onclik="return Confirmar('¿Desea borrar?');" class ="btn btn-danger" type="submit" name="accion">ELIMINAR</button>
                        
                        </form>
                    </td>
                    
                    
                </tr>



            <?php }?>
        </table>
   
    
 
    </div>
<?php if($mostrarModal){?>
    <script>
        $('#exampleModal').modal('show');
    </script>
<?php }?>
<script>
    function Confirmar(Mensaje){
        return (confirm(Mensaje))?true:false;
    }
</script>

        
    </div>
</body>
</html>
