<?php

include("Conexion/config.php");
include("Conexion/conexion.php");
include 'carrito.php';
//include 'login/codlogin.php';
//include 'templates/cabecera.php';
//include 'mostrarCarrito.php'
?>
<?php 

$txtID=(isset($_POST['txtID']))?$_POST['txtID']:"";
$txtTotal=(isset($_POST['txtTotal']))?$_POST['txtTotal']:"";
$txtTarjeta=(isset($_POST['txtTarjeta']))?$_POST['txtTarjeta']:"";
$txtTitular=(isset($_POST['txtTitular']))?$_POST['txtTitular']:"";
$txtDireccion=(isset($_POST['txtDireccion']))?$_POST['txtDireccion']:"";

$accion=(isset($_POST['accion']))?$_POST['accion']:"";

$txtTotal=0;
foreach($_SESSION['CARRITO'] as $indice=>$principal){

  $txtTotal=$txtTotal+($principal['PRECIO']*$principal['CANTIDAD']);
  
}

$txtTitular=$_SESSION['USUARIO'][0]['NOMBRE'];

switch($accion){

  case "btnFinalizar":

    $sentencia=$pdo->prepare("INSERT INTO factura(titular,direccion,fecha,total) 
    VALUES (:titular,:direccion,:fecha,:total) ");

    $Fecha= date("y-m-d");   
    
    $sentencia->bindParam(':titular',$txtTitular);
    $sentencia->bindParam(':direccion',$txtDireccion);
    $sentencia->bindParam(':fecha',$Fecha);
    $sentencia->bindParam(':total',$txtTotal);
    $sentencia->execute();

    $id=$pdo->prepare("SELECT MAX(IdFactura) as id FROM factura");
    $id->execute();
    $factura=$id->fetch(PDO::FETCH_LAZY);

    $txtID=$factura['id'];

  

    foreach($_SESSION['CARRITO'] as $indice=>$principal){
      $sentencia=$pdo->prepare("INSERT INTO facturacion(IdFactura,producto,precio,cantidad,total) 
      VALUES (:IdFactura,:producto,:precio,:cantidad,:total) ");
      $unidad =$_SESSION['CARRITO'][$indice]['CANTIDAD']*$_SESSION['CARRITO'][$indice]['PRECIO'];


   
    
      $sentencia->bindParam(':IdFactura',$txtID);
      $sentencia->bindParam(':producto',$_SESSION['CARRITO'][$indice]['NOMBRE']);
      $sentencia->bindParam(':precio',$_SESSION['CARRITO'][$indice]['PRECIO']);
      $sentencia->bindParam(':cantidad',$_SESSION['CARRITO'][$indice]['CANTIDAD']);
      $sentencia->bindParam(':total',$unidad);
     
      $sentencia->execute();

      $sentencia=$pdo->prepare("INSERT INTO venta(IdUsuario,IdProducto) 
      VALUES (:IdUsuario,:IdProducto) ");   
    
      $sentencia->bindParam(':IdUsuario',$_SESSION['USUARIO'][0]['ID']);
      $sentencia->bindParam(':IdProducto',$_SESSION['CARRITO'][$indice]['ID']);
     
      $sentencia->execute();

    
      
    }

    header('Location: terminar.php');


    //echo "finalizar";
    //echo $txtTitular;

  break;

  case "btnVolver":

    header('Location: mostrarCarrito.php');

  break;


}





?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
</head>
<body>
  <br>
  <br>
  <div class="container">
    <form action="" method="post">
    
        <div class="card mx-auto " style="width: 40rem; justify-content: center; " >
          <div class="card-header text-center">
          <h2>Pago Tarjeta</h2>
          </div>
          <div class="card-body">
            <div class="form-group">        
            <input type="hidden" name="txtID" value="<?php echo $txtID?>" placeholder="" class="form-control" id="txtID">
            </div>

            <div class="form-group">
            <label for="">TOTAL A PAGAR:</label>
            <input type="text" name="txtTotal" value="<?php echo $txtTotal?>" placeholder="" class="form-control" id="txtTotal">
            </div>

            <div class="form-group">
            <label for="">Tarjeta:</label>
            <input type="text" name="txtTarjeta"  placeholder="" class="form-control" id="txtTarjeta">
            </div>

            <div class="form-group">
            <label for="">Titular:</label>
            <input type="text" name="txtTitular" value="<?php echo $txtTitular?>" placeholder="" class="form-control" id="txtTitular">
            </div>

            <div class="form-group">
            <label for="">Direccion:</label>
            <input type="text" name="txtDireccion"  placeholder="" class="form-control" id="txtDireccion">
            </div>

          </div>
          <div class="card-footer text-muted">
            <div class="float-left">
              <button value="btnVolver" type="submit" name="accion" class="btn btn-warning btn-lg btn.block"><< VOLVER</button>
            </div>
            <div class="float-right">
              <button value="btnFinalizar" type="submit" name="accion" class="btn btn-success btn-lg btn.block">FINALIZAR >></button>
            </div>         
          </div>
        </div>

 



    </form>

  </div>
  
</body>
</html>